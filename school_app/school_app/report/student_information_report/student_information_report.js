// Copyright (c) 2016, shiela and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Student Information Report"] = {
	"filters": [
		{
			"fieldname": "courses",
            "label": __("Course"),
            "fieldtype": "Link",
            "options": "Courses"
		}
	]
}
