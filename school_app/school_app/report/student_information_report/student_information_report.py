# Copyright (c) 2013, shiela and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns, data = [], []

	columns = [
		{"label": "ID", "width": 100, "fieldname": "studentid"},
		{"label": "First Name", "width": 150, "fieldname": "firstname"},
		{"label": "Middle Name", "width": 150, "fieldname": "middlename"},
		{"label": "Last Name", "width": 150, "fieldname": "lastname"},
		{"label": "Full Name", "width": 200, "fieldname": "fullname"},
		{"label": "Birthday", "width": 120, "fieldname": "birth"},
		{"label": "Age", "width": 50, "fieldname": "age"},
		{"label": "Courses", "width": 200, "fieldname": "course"}
	]
	if filters.get("courses"):
		myquery = frappe.db.sql(""" SELECT * FROM `tabStudent Information` where courses=%s""",(filters.get("courses")), as_dict=True)
	else:
		myquery = frappe.db.sql(""" SELECT * FROM `tabStudent Information`""", as_dict=True)
	for i in range(len(myquery)):
		data.append({
			"studentid": myquery[i]['student_id'],
			"firstname": myquery[i]['first_name'],
			"middlename": myquery[i]['middle_name'],
			"lastname": myquery[i]['last_name'],
			"fullname": myquery[i]['full_name'],
			"birth": myquery[i]['birthday'],
			"age": myquery[i]['age'],
			"course": myquery[i]['courses']
		})

	return columns, data