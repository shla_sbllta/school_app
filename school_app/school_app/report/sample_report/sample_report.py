# Copyright (c) 2013, shiela and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns, data = [], []

	columns = [
		{"label": "ID", "width": 200, "fieldname": "courseID"},
		{"label": "Name", "width": 200, "fieldname": "coursename"}
	]

	myquery = frappe.db.sql(""" SELECT * FROM tabCourses""", as_dict=True)
	for i in range (len(myquery)):
		data.append({
			"courseID": (myquery[i]['courseid']),
			"coursename": (myquery[i]['name'])
		})

	return columns, data