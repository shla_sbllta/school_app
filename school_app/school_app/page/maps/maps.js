frappe.pages['maps'].on_page_load = function(wrapper) {
	new MyPage(wrapper);
}

MyPage = Class.extend({
	init: function(wrapper) {
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			title: 'Maps',
			single_column: true
		});
		this.make();
	},
	make: function() {
		$(frappe.render_template("maps", this)).appendTo(this.page.main);

		var mymap = L.map('mapid').setView([8.4873, 124.6435], 13);

		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
       		attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    	}).addTo(mymap);

		var vac = L.circle([8.4974,124.6405], {
			color: 'green',
			fillColor: 'green',
			radius: 50,
			opacity: 1
		}).addTo(mymap);
		var vac = L.circle([8.4942,124.6374], {
			color: 'green',
			fillColor: 'green',
			radius: 50,
			opacity: 1
		}).addTo(mymap);
		var vac = L.circle([8.4977,124.6313], {
			color: 'green',
			fillColor: 'green',
			radius: 50,
			opacity: 1
		}).addTo(mymap);


		var unvac = L.circle([8.5042,124.6366], {
			color: 'red',
			fillColor: 'red',
			radius: 50,
			opacity: 1
		}).addTo(mymap);
		var unvac = L.circle([8.5050,124.6291], {
			color: 'red',
			fillColor: 'red',
			radius: 50,
			opacity: 1
		}).addTo(mymap);
		var unvac = L.circle([8.4961,124.6364], {
			color: 'red',
			fillColor: 'red',
			radius: 50,
			opacity: 1
		}).addTo(mymap);

		setTimeout(function(){ mymap.invalidateSize(true)}, 400);
	}
})