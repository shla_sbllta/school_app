# -*- coding: utf-8 -*-
# Copyright (c) 2019, shiela and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

import requests

class Comments(Document):
	def validate(self):
			print ("---------------------------------------	")
			url = "https://http://localhost:3000/comments"
			mydata = {
				'id': self.post_id,
				'email': self.email,
				'body': self.body
			}
			response = requests.post(url, data=mydata)
			print (response)

			# response = requests.get(url)
			# jsondata = response.json()

			# self.post_id = jsondata[0]["id"]
			# self.email = jsondata[0]["email"]
			# self.body = jsondata[0]["body"]