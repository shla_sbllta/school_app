// Copyright (c) 2019, shiela and contributors
// For license information, please see license.txt

frappe.ui.form.on('Courses', {
	refresh: function(frm) {

	}
});

cur_frm.cscript.studentid = function(frm,cdt,cdn) {
	var df = locals[cdt][cdn] //na trigger na row
	console.log(df)
	frappe.call({
		method:"school_app.school_app.doctype.courses.courses.test", //path padulong sa method name sa server (in this case: test)
		args: {
			"df_row_data": df, //gisend sa server ang natrigger na row
		},
		callback: function (r) {
			console.log("DATAAAAAAAAAAAAAAAAAAAAA FROM STUDENT INFORMATION")
			console.log(r)
			df.first_name = r.message[0].first_name
			df.middle_name = r.message[0].middle_name
			df.last_name = r.message[0].last_name
			df.full_name = r.message[0].full_name
			df.birthday = r.message[0].birthday
			df.age = r.message[0].age
			df.course = r.message[0].courses
			cur_frm.refresh_field("students_enrolled")
        }
	})
}

cur_frm.cscript.subjectid = function (frm,cdt,cdn) {
	var subjectrow = locals[cdt][cdn]
	console.log(subjectrow) // gi log para makita ang trigger
	frappe.call({
		method: "school_app.school_app.doctype.courses.courses.subject",
		args: {
			"data_from_subjectID_row": subjectrow
		},
		callback: function (e) {
			console.log("NAPASA GIKAN SA SERVER")
			console.log(e)
			//e call ang row tas sudlan atong napasa nga data gkan sa server
			subjectrow.subject_name = e.message[0].subject_name
			cur_frm.refresh_field("list_of_subjects")

		}
	})
}






//----------------- CUSTOM SCRIPT TO CORE DOCTYPE
// cur_frm.cscript.birthday = function(frm,cdt,cdn) {
// 	var myrow = locals[cdt][cdn] //na trigger na row
// 	console.log(myrow)
// 	frappe.call({
// 		method:"school_app.test.client_server", //path padulong sa method name sa server (in this case: test)
// 		args: {
// 			"my_row_data": myrow, //gisend sa server ang natrigger na row
// 		},
// 		callback: function (r) {
// 			console.log("DATAAAAAAAAAAAAAAAAAAAAA FROM server")
// 			console.log(r)
// 			var age = getage(r.message['birthday']);
// 			myrow.age = age;
// 			cur_frm.refresh_field("age")
//         }
// 	})
// }
// function getage(birth){
// 	var mybirth = new Date(birth);
// 	var cur = new Date()
// 	if (mybirth.getMonth()<=cur.getMonth() && mybirth.getDate()<=cur.getDate()){
// 		console.log("nagbirthday na")
// 		return (cur.getFullYear()-mybirth.getFullYear())
//     	}else {
// 		console.log("wala pa")
// 		var res = (cur.getFullYear()-mybirth.getFullYear()-1)
// 		if (res == -1){
// 			return res = 0
// 		}else {
// 			return res
//         }
//     }
// }