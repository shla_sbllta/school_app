# -*- coding: utf-8 -*-
# Copyright (c) 2019, shiela and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

from datetime import date
import datetime

class StudentInformation(Document):
	def validate(self):
		self.full_name=(self.first_name+" "+self.middle_name+" "+self.last_name)
		# self.full_name ="hello world!"

		today = date.today()
		y = datetime.datetime.strptime(self.birthday, "%Y-%m-%d").strftime("%Y")
		m = datetime.datetime.strptime(self.birthday, "%Y-%m-%d").strftime("%m")
		d = datetime.datetime.strptime(self.birthday, "%Y-%m-%d").strftime("%d")
		birth = date(int(y), int(m), int(d))
		age = today.year - birth.year - ((today.month, today.day) < (birth.month, birth.day))
		self.age = age